package test;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.List;

public class TestHelloAntlr {

    @Test
    public void test1(){
        String entry = "hellow";
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(entry);
        HelloLexer lexer = new HelloLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        HelloParser parser = new HelloParser(tokens);

        log("Test");
        /*log(StringUtils.join(
                parser.getTokenNames(), " - "));
        log(parser.getRuleNames());

        log(parser.getCurrentToken().getText());*/
        ParseTreeWalker walker = new ParseTreeWalker();
        HelloBaseListener listener = new MyHelloBaseListener();
        walker.walk(listener, parser.r());
        log(" Ah");
        parser.addParseListener(listener);
        log(" Beh");
        parser.dumpDFA();
        ;

    }


    private void log(String s)
    {
        System.out.println(s);
    }

    private void log(String[] s)
    {
        System.out.println("[" + StringUtils.join(s, ", ") + "]");
    }

    private void log(List<String> s)
    {
        log((String[]) s.toArray());
    }

    private class MyHelloBaseListener extends HelloBaseListener {
        @Override
        public void enterR(HelloParser.RContext ctx) {
            super.enterR(ctx);    //To change body of overridden methods use File | Settings | File Templates.
            log("ENTER R: " + ctx.getText());
        }

        @Override
        public void exitR(HelloParser.RContext ctx) {
            super.exitR(ctx);    //To change body of overridden methods use File | Settings | File Templates.
            log("EXIT R: " + ctx.getText());
        }

        @Override
        public void enterEveryRule(ParserRuleContext ctx) {
            super.enterEveryRule(ctx);    //To change body of overridden methods use File | Settings | File Templates.
            log("ENTER EVERY RULE: " + ctx.getText());
        }

        @Override
        public void exitEveryRule(ParserRuleContext ctx) {
            super.exitEveryRule(ctx);    //To change body of overridden methods use File | Settings | File Templates.
            log("EIT EVERY RULE: " + ctx.getText());
        }

        @Override
        public void visitTerminal(TerminalNode node) {
            super.visitTerminal(node);    //To change body of overridden methods use File | Settings | File Templates.
            log("VISIT TERMINAL: " + node.getSymbol());
        }

        @Override
        public void visitErrorNode(ErrorNode node) {
            super.visitErrorNode(node);    //To change body of overridden methods use File | Settings | File Templates.
            log("ISIT ERROR NODE: " + node.getSymbol());
        }
    }
}
